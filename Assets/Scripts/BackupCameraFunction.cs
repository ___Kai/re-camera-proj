using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System.Linq;
public class BackupCameraFunction : MonoBehaviour
{
    private CinemachineVirtualCamera prevCam;
    public CinemachineVirtualCamera activeCam;
    public GameObject cameraSwapTrigger;
    public int priority1;
    public int priority2;
    public List<CinemachineVirtualCamera> cameraList = new List<CinemachineVirtualCamera>(); 

    // Start is called before the first frame update
    void Start()
    {
        cameraSwapTrigger.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerExit(Collider other)
    {
        Debug.Log("This is working... apparently");
        for (int i = 0; i < cameraList.Count; i++)
        {
            cameraList[i].Priority = priority2;
        }
        activeCam.Priority = priority1;
        cameraSwapTrigger.SetActive(true);
        //prevCam = FindObjectOfType<CinemachineVirtualCamera>();
        //activeCam.gameObject.SetActive(true);
        //prevCam.gameObject.SetActive(false);
    }
}
